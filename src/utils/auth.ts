interface UserLogged {
  id: number;
  username: string;
  role: string;
}

export function getUserToken(): string | null {
  let tokenData: any;
  if (typeof window !== "undefined") {
    tokenData = localStorage.getItem("client-token");
  }

  if (tokenData) {
    try {
      const parsedData = JSON.parse(tokenData);
      return parsedData.token;
    } catch (error) {
      console.error("Failed to parse token data:", error);
      return null;
    }
  }
  return null;
}

export const userAuthenticated: UserLogged =
  typeof window !== "undefined"
    ? JSON.parse(window.localStorage.getItem("client-user") ?? "{}")
    : null;
