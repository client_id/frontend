export interface User {
  id: number;
  username: string;
  role: string;
  deleted?: boolean;
}

export interface PayloadUser {
  id?: number;
  username: string;
  password: string;
  role: string;
}
