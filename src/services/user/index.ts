import axios from "..";

import { Pagination } from "../types";
import { User, PayloadUser } from "./types";
export type { User, PayloadUser };

const url = "/users";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  get: (): Promise<User[]> => {
    return axios.get(`${url}`);
  },
  topUsers: (): Promise<{ username: string; clientCount: number }[]> => {
    return axios.get(`${url}/top-users`);
  },
  create: (data: PayloadUser): Promise<User> => {
    return axios.post("auth/signup", data);
  },
  update: (id: number, data: PayloadUser): Promise<User> => {
    return axios.put(`${url}/${id}`, data);
  },
  delete: (id: number): Promise<any> => {
    return axios.delete(`${url}/${id}`);
  },
};
