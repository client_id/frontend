import axios from ".";

// eslint-disable-next-line import/no-anonymous-default-export
export function setAuthorizationHeader(token = null) {
  if (token) axios.defaults.headers.common.authorization = "Bearer " + token;
  else delete axios.defaults.headers.common.authorization;
}
