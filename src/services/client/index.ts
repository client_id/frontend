import axios from "..";

import { Pagination } from "../types";
import { Client, PayloadClient, ClientProgress } from "./types";
export type { Client, PayloadClient, ClientProgress };

const url = "/clients";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  get: (): Promise<Client[]> => {
    return axios.get(`${url}`);
  },
  detail: (id: number): Promise<Client> => {
    return axios.get(`${url}/${id}`);
  },
  create: (data: PayloadClient): Promise<Client> => {
    return axios.post(url, data);
  },
  update: (id: number, data: PayloadClient): Promise<Client> => {
    return axios.put(`${url}/${id}`, data);
  },
  // progress: (id: number, data: ProgressClient): Promise<Client> => {
  //   return axios.put(`${url}/${id}`, data);
  // },
  delete: (id: number): Promise<any> => {
    return axios.delete(`${url}/${id}`);
  },
  getProgress: (id: number): Promise<ClientProgress[]> => {
    return axios.get(`${url}/${id}/progress`);
  },
  addProgress: (id: number, data: ClientProgress): Promise<Client> => {
    return axios.put(`${url}/${id}/state`, data);
  },
  getStateCount: (id?: number): Promise<any> => {
    if (id) {
      return axios.get(`${url}/state-count/${id}`);
    } else {
      return axios.get(`${url}/state-count`);
    }
  },
  getUserClientCount: (id?: number): Promise<any> => {
    if (id) {
      return axios.get(`${url}/user-client-counts/${id}`);
    } else {
      return axios.get(`${url}/user-client-counts`);
    }
  },
};
