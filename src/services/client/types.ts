export interface Client {
  id: number;
  name: string;
  sales_id: number;
  sales_name: string;
  position: string;
  company: string;
  industry_type: string;
  phone: string;
  email: string;
  source_lit: string;
  state: string;
}

export interface PayloadClient {
  id?: number;
  name: string;
  position: string;
  company: string;
  industry_type: string;
  phone: string;
  email: string;
  source_lit: string;
  state: string;
  sales_id: number;
}

export interface ClientProgress {
  state: string;
  date: any;
  notes?: string;
}
