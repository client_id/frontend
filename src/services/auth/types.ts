export interface PayloadLogin {
  username: string;
  password: string;
}
