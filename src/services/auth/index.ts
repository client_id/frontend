import axios from "..";

import { PayloadLogin } from "./types";
export type { PayloadLogin };

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  login: (data: PayloadLogin) => {
    return axios.post<PayloadLogin, any>("/auth/login", data);
  },
  logout: (): Promise<any> => {
    return axios.delete("/logout");
  },
};
