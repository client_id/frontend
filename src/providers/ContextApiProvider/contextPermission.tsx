import { useState, createContext } from "react";

interface Permissions {
  [key: string]: {
    [key: string]: string;
  };
}

interface ContextProps {
  permissions: Permissions;
  setPermissions: React.Dispatch<React.SetStateAction<Permissions>>;
}

export const ContextPermission = createContext<ContextProps>({
  permissions: {},
  setPermissions: () => {},
});

export default function AppProvider(props: any) {
  const [permissions, setPermissions] = useState<Permissions>({});

  return (
    <ContextPermission.Provider value={{ permissions, setPermissions }}>
      {props.children}
    </ContextPermission.Provider>
  );
}
