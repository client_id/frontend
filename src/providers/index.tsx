"use client";

import { useEffect, useState } from "react";
import { usePathname } from "next/navigation";

export { ContextNotfication, ContextPermission } from "./ContextApiProvider";
export { default as MaterialUiProvider } from "./MaterialUiProvider";
export { default as ReactQueryProvider } from "./ReactQueryProvider";

import {
  MaterialUiProvider,
  ReactQueryProvider,
  ContextNotfication,
  ContextPermission,
} from "@providers/index";

import { setAuthorizationHeader } from "@services/setAuthorizationHeader";

import Layout from "@/app/(DashboardLayout)/layout";

export default function useLayout(props: { children: React.ReactNode }) {
  const [ready, setReady] = useState(false);

  const routePathname = usePathname();
  const isAuthPage = routePathname.split("/")[1] === "login";

  useEffect(() => {
    const token: any = localStorage.getItem("client-token");

    if (token) {
      let newToken: any = JSON.parse(token);
      setAuthorizationHeader(newToken.token);
    }

    setReady(true);
  }, []);

  return (
    <MaterialUiProvider>
      <ContextNotfication>
        <ContextPermission>
          <ReactQueryProvider>
            {/* {ready &&
              (isAuthPage ? props.children : <Layout>{props.children}</Layout>)} */}
            {ready && props.children}
          </ReactQueryProvider>
        </ContextPermission>
      </ContextNotfication>
    </MaterialUiProvider>
  );
}
