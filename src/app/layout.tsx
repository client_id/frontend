"use client";

import UseProvider from "@providers/index";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <UseProvider>{children}</UseProvider>
      </body>
    </html>
  );
}
