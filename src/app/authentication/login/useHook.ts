"use client";

import { useState, useContext, useEffect } from "react";
import { useRouter } from "next/navigation";

import { useMutation } from "@tanstack/react-query";

import { ContextNotfication } from "@providers/ContextApiProvider/contextNotfication";

import auth, { PayloadLogin } from "@services/auth";

import { setAuthorizationHeader } from "@services/setAuthorizationHeader";
import { getUserToken } from "@utils/auth";

export default function StationsComponentHook() {
  const { loading, setLoading, setSnackbar } = useContext(ContextNotfication);

  const router = useRouter();

  let initForm = { username: "", password: "" };

  const [form, setForm] = useState<PayloadLogin>(initForm);
  const [error, setError] = useState<any>("");
  const [showPassword, setShowPassword] = useState(false);

  const [isLoginProcess, setIsLoginProcess] = useState<boolean>(false);

  useEffect(() => {
    if (getUserToken() && !isLoginProcess) {
      setLoading(true);
      router.replace("/");
    }
  });

  const loginMutate = useMutation(
    async (data: PayloadLogin) => auth.login(data),
    {
      onSuccess: (res: any) => onApiSuccess(res),
      onError: (error: any) => validate(error),
    }
  );

  const handleChange = (e: any) => {
    const { name, value } = e.target;

    setForm((f) => ({
      ...f,
      [name]: value,
    }));

    setError((f: any) => ({
      ...f,
      [name]: "",
    }));
  };

  function onApiSuccess(res: any) {
    setIsLoginProcess(true);
    setLoading(true);

    setAuthorizationHeader(res.token);

    localStorage.setItem("client-token", JSON.stringify({ token: res.token }));

    localStorage.setItem(
      "client-user",
      JSON.stringify({
        id: res.id,
        username: res.username,
        role: res.role,
      })
    );

    const timer = setTimeout(() => {
      window.location.reload();

      clearTimeout(timer);
    }, 2000);
  }

  function validate(res: any) {
    if (res.status === 422) {
      const newError = { ...error };

      res.data.errors.map((item: any) => {
        newError[item.field] = item.message;
      });

      setError(newError);
      setSnackbar({
        open: true,
        text: "Unprocessable Content",
        variant: "error",
      });
    }
    if (res.status === 400) {
      const newError = { ...error };
      newError["password"] = res.data.errors[0].message;
      setError(newError);
    } else {
      setSnackbar({
        open: true,
        text: res.data.message,
        variant: "error",
      });
    }
  }

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    loginMutate.mutate(form);
  };

  let loadingPage = loginMutate.isLoading || loading;

  return {
    form,
    error,
    handleChange,
    handleSubmit,
    loading: loadingPage,
    showPassword,
    setShowPassword,
  };
}
