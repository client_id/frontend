import React from "react";
import { Select, MenuItem, Box, Avatar, Grid, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import DashboardCard from "@/app/(DashboardLayout)/components/shared/DashboardCard";

import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import EditCalendarOutlinedIcon from "@mui/icons-material/EditCalendarOutlined";
import AddIcCallOutlinedIcon from "@mui/icons-material/AddIcCallOutlined";
import DisplaySettingsOutlinedIcon from "@mui/icons-material/DisplaySettingsOutlined";
import EditLocationOutlinedIcon from "@mui/icons-material/EditLocationOutlined";
import AssignmentOutlinedIcon from "@mui/icons-material/AssignmentOutlined";
import AssignmentTurnedInOutlinedIcon from "@mui/icons-material/AssignmentTurnedInOutlined";
import CheckCircleOutlineOutlinedIcon from "@mui/icons-material/CheckCircleOutlineOutlined";
import ContentPasteOffOutlinedIcon from "@mui/icons-material/ContentPasteOffOutlined";

interface Props {
  stateCount: any;
}

export default function ClientState(props: Props) {
  // chart color
  const theme = useTheme();
  const primary = theme.palette.primary.main;
  const warning = theme.palette.warning.main;
  const error = theme.palette.error.main;

  return (
    <>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center">
            <Avatar sx={{ bgcolor: primary, width: 65, height: 65, mr: 3 }}>
              <PersonOutlineOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Box>
              <Typography
                variant="body1"
                color="gray"
                fontWeight={400}
                whiteSpace="nowrap"
              >
                New
              </Typography>
              <Typography variant="h2" fontWeight="bold">
                {props.stateCount?.["New"] ?? 0}
              </Typography>
            </Box>
          </Box>
        </DashboardCard>
      </Grid>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center">
            <Avatar sx={{ bgcolor: warning, width: 65, height: 65, mr: 3 }}>
              <AddIcCallOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Box>
              <Typography
                variant="body1"
                color="gray"
                fontWeight={400}
                whiteSpace="nowrap"
              >
                Folup
              </Typography>
              <Typography variant="h2" fontWeight="bold">
                {props.stateCount?.["Folup"] ?? 0}
              </Typography>
            </Box>
          </Box>
        </DashboardCard>
      </Grid>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center">
            <Avatar sx={{ bgcolor: error, width: 65, height: 65, mr: 3 }}>
              <EditCalendarOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Box>
              <Typography
                variant="body1"
                color="gray"
                fontWeight={400}
                whiteSpace="nowrap"
              >
                Presentation
              </Typography>
              <Typography variant="h2" fontWeight="bold">
                {props.stateCount?.["Need Presentation"] ?? 0}
              </Typography>
            </Box>
          </Box>
        </DashboardCard>
      </Grid>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center">
            <Avatar sx={{ bgcolor: primary, width: 65, height: 65, mr: 3 }}>
              <DisplaySettingsOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Box>
              <Typography
                variant="body1"
                color="gray"
                fontWeight={400}
                whiteSpace="nowrap"
              >
                Need Demo
              </Typography>
              <Typography variant="h2" fontWeight="bold">
                {props.stateCount?.["Need Demo"] ?? 0}
              </Typography>
            </Box>
          </Box>
        </DashboardCard>
      </Grid>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center">
            <Avatar sx={{ bgcolor: warning, width: 65, height: 65, mr: 3 }}>
              <EditLocationOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Box>
              <Typography
                variant="body1"
                color="gray"
                fontWeight={400}
                whiteSpace="nowrap"
              >
                Need Visit
              </Typography>
              <Typography variant="h2" fontWeight="bold">
                {props.stateCount?.["Need Visit"] ?? 0}
              </Typography>
            </Box>
          </Box>
        </DashboardCard>
      </Grid>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center">
            <Avatar sx={{ bgcolor: error, width: 65, height: 65, mr: 3 }}>
              <AssignmentOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Box>
              <Typography
                variant="body1"
                color="gray"
                fontWeight={400}
                whiteSpace="nowrap"
              >
                Quotation
              </Typography>
              <Typography variant="h2" fontWeight="bold">
                {props.stateCount?.["Quotation"] ?? 0}
              </Typography>
            </Box>
          </Box>
        </DashboardCard>
      </Grid>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center">
            <Avatar sx={{ bgcolor: primary, width: 65, height: 65, mr: 3 }}>
              <AssignmentTurnedInOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Box>
              <Typography
                variant="body1"
                color="gray"
                fontWeight={400}
                whiteSpace="nowrap"
              >
                Transaction
              </Typography>
              <Typography variant="h2" fontWeight="bold">
                {props.stateCount?.["Transaction"] ?? 0}
              </Typography>
            </Box>
          </Box>
        </DashboardCard>
      </Grid>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center">
            <Avatar sx={{ bgcolor: warning, width: 65, height: 65, mr: 3 }}>
              <CheckCircleOutlineOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Box>
              <Typography
                variant="body1"
                color="gray"
                fontWeight={400}
                whiteSpace="nowrap"
              >
                Done
              </Typography>
              <Typography variant="h2" fontWeight="bold">
                {props.stateCount?.["Done"] ?? 0}
              </Typography>
            </Box>
          </Box>
        </DashboardCard>
      </Grid>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center">
            <Avatar sx={{ bgcolor: error, width: 65, height: 65, mr: 3 }}>
              <ContentPasteOffOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Box>
              <Typography
                variant="body1"
                color="gray"
                fontWeight={400}
                whiteSpace="nowrap"
              >
                Failed
              </Typography>
              <Typography variant="h2" fontWeight="bold">
                {props.stateCount?.["Failed"] ?? 0}
              </Typography>
            </Box>
          </Box>
        </DashboardCard>
      </Grid>
    </>
  );
}
