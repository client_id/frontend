import React from "react";
import { Select, MenuItem, Box, Avatar, Grid, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import DashboardCard from "@/app/(DashboardLayout)/components/shared/DashboardCard";

import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import EditCalendarOutlinedIcon from "@mui/icons-material/EditCalendarOutlined";
import AddIcCallOutlinedIcon from "@mui/icons-material/AddIcCallOutlined";
import DisplaySettingsOutlinedIcon from "@mui/icons-material/DisplaySettingsOutlined";
import EditLocationOutlinedIcon from "@mui/icons-material/EditLocationOutlined";
import AssignmentOutlinedIcon from "@mui/icons-material/AssignmentOutlined";
import AssignmentTurnedInOutlinedIcon from "@mui/icons-material/AssignmentTurnedInOutlined";
import CheckCircleOutlineOutlinedIcon from "@mui/icons-material/CheckCircleOutlineOutlined";
import ContentPasteOffOutlinedIcon from "@mui/icons-material/ContentPasteOffOutlined";

interface Props {
  count: {
    user: number;
    client: number;
  };
}

export default function ClientState(props: Props) {
  // chart color
  const theme = useTheme();
  const primary = theme.palette.primary.main;
  const warning = theme.palette.warning.main;
  const error = theme.palette.error.main;

  return (
    <>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center" flexDirection="column">
            <Avatar sx={{ bgcolor: primary, width: 65, height: 65, mb: 1 }}>
              <PersonOutlineOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Typography variant="h2" fontWeight="bold" sx={{ mb: 1 }}>
              {props.count?.client ?? 0}
            </Typography>
            <Typography
              variant="body1"
              color="gray"
              fontWeight={400}
              whiteSpace="nowrap"
            >
              Client Registered
            </Typography>
          </Box>
        </DashboardCard>
      </Grid>
      <Grid item xs={4}>
        <DashboardCard>
          <Box display="flex" alignItems="center" flexDirection="column">
            <Avatar sx={{ bgcolor: error, width: 65, height: 65, mb: 1 }}>
              <PersonOutlineOutlinedIcon sx={{ width: 40, height: 40 }} />
            </Avatar>
            <Typography variant="h2" fontWeight="bold" sx={{ mb: 1 }}>
              {props.count?.user ?? 0}
            </Typography>
            <Typography
              variant="body1"
              color="gray"
              fontWeight={400}
              whiteSpace="nowrap"
            >
              Sales Active
            </Typography>
          </Box>
        </DashboardCard>
      </Grid>
    </>
  );
}
