"use client";
import { Grid, Box } from "@mui/material";
import PageContainer from "@/app/(DashboardLayout)/components/container/PageContainer";
// components
import SalesOverview from "@/app/(DashboardLayout)/components/dashboard/SalesOverview";
import YearlyBreakup from "@/app/(DashboardLayout)/components/dashboard/YearlyBreakup";
import TopSales from "@/app/(DashboardLayout)/components/dashboard/TopSales";
import RecentClient from "@/app/(DashboardLayout)/components/dashboard/RecentClient";
import ProductPerformance from "@/app/(DashboardLayout)/components/dashboard/ProductPerformance";
import Blog from "@/app/(DashboardLayout)/components/dashboard/Blog";
import MonthlyEarnings from "@/app/(DashboardLayout)/components/dashboard/MonthlyEarnings";

import ClientState from "@/app/(DashboardLayout)/components/dashboard/ClientState";
import ClientSalesCount from "@/app/(DashboardLayout)/components/dashboard/ClientSalesCount";

import useHooks from "./useHook";

const Dashboard = () => {
  const hooks = useHooks();

  return (
    <PageContainer title="Dashboard" description="this is Dashboard">
      <Box>
        <Grid container spacing={3}>
          <Grid item container xs={9} spacing={3} gridAutoRows="1fr">
            <ClientState stateCount={hooks.stateCount} />
          </Grid>
          <Grid
            item
            container
            xs={3}
            direction="column"
            spacing={3}
            gridAutoRows="1fr"
          >
            <ClientSalesCount count={hooks.userClientCount} />
          </Grid>
          <Grid item xs={7}>
            <RecentClient clients={hooks.clients ?? []} />
          </Grid>
          <Grid item xs={5}>
            <TopSales sales={hooks.topUsers ?? []} />
          </Grid>
        </Grid>
      </Box>
    </PageContainer>
  );
};

export default Dashboard;
