"use client";

import Link from "next/link";
import PageContainer from "@/app/(DashboardLayout)/components/container/PageContainer";
import DashboardCard from "@/app/(DashboardLayout)/components/shared/DashboardCard";

import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Button,
} from "@mui/material";

import Form from "./components/form";
import useHook from "./useHook";

const ClientAddPage = () => {
  const hooks = useHook();

  return (
    <PageContainer title="Client Page" description="this is Client page">
      <DashboardCard title="Daftar Client">
        <Box sx={{ overflow: "auto", width: { xs: "280px", sm: "auto" } }}>
          <Button
            variant="contained"
            component={Link}
            href="/clients/add"
            disableElevation
            color="primary"
          >
            Tambah Client
          </Button>
          <Table
            aria-label="simple table"
            sx={{
              whiteSpace: "nowrap",
              mt: 2,
            }}
          >
            <TableHead>
              <TableRow>
                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    No
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    Nama
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    Perusahaan
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    Sales
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    State
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography variant="subtitle2" fontWeight={600}>
                    Action
                  </Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {hooks.clients?.map((item, index) => (
                <TableRow
                  key={item.name}
                  hover
                  sx={{ cursor: "pointer", textDecoration: "none" }}
                  component={Link}
                  href={`/clients/${item.id}`}
                >
                  <TableCell>
                    <Typography
                      sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                      }}
                    >
                      {index + 1}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <Box>
                        <Typography variant="subtitle2" fontWeight={600}>
                          {item.name}
                        </Typography>
                        <Typography
                          color="textSecondary"
                          sx={{
                            fontSize: "13px",
                          }}
                        >
                          {item.position}
                        </Typography>
                      </Box>
                    </Box>
                  </TableCell>
                  <TableCell>
                    <Typography
                      color="textSecondary"
                      variant="subtitle2"
                      fontWeight={400}
                    >
                      {item.company}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      color="textSecondary"
                      variant="subtitle2"
                      fontWeight={400}
                    >
                      {item.sales_name}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      color="textSecondary"
                      variant="subtitle2"
                      fontWeight={400}
                    >
                      {item.state}
                    </Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Button
                      variant="contained"
                      onClick={(e) => {
                        e.stopPropagation();
                        e.preventDefault();
                        hooks.setForm(item);
                        hooks.setOpenDialog({ open: true, type: "Delete" });
                      }}
                      disableElevation
                      color="error"
                    >
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </DashboardCard>
      <Form
        data={hooks.form}
        openDialog={hooks.openDialog}
        onCloseDialog={(e) => {
          hooks.setOpenDialog(e);
          hooks.setForm(hooks.initForm);
        }}
      />
    </PageContainer>
  );
};

export default ClientAddPage;
