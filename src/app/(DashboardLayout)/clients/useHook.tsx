"use client";
import { useState, useContext } from "react";
import { ContextNotfication } from "@providers/ContextApiProvider/contextNotfication";

import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";

import serviceApi, { Client, PayloadClient } from "@services/client";
import { userAuthenticated } from "@/utils/auth";

export default function StationsHook() {
  const { setSnackbar, setLoading } = useContext(ContextNotfication);

  const [clientData, setClientData] = useState<Client[]>([]);

  const [openDialog, setOpenDialog] = useState({
    open: false,
    type: "",
  });

  let initForm: Client = {
    id: 0,
    name: "",
    position: "",
    company: "",
    industry_type: "",
    phone: "",
    email: "",
    source_lit: "",
    state: "New",
    sales_id: 0,
    sales_name: "",
  };

  const [page, setPage] = useState(1);
  const [form, setForm] = useState<Client>(initForm);

  const [searchField, setSearchField] = useState<string>("");

  const isAdmin = userAuthenticated.role === "Admin";

  const clients = useQuery(["clients"], async () => serviceApi.get(), {
    onSuccess: (res) => {
      if (isAdmin) {
        setClientData(res);
      } else {
        let v = res.filter((item) => item.sales_id === userAuthenticated.id);
        setClientData(v);
      }
    },
    onError: (error: any) =>
      setSnackbar({
        open: true,
        text: error.data.message,
        variant: "error",
      }),
  });

  function handleSerach(val: string) {
    setPage(1);
    setSearchField(val);

    // clients.refetch();
  }

  return {
    clients: clientData,
    openDialog,
    setOpenDialog,
    initForm,
    form,
    setForm,
    page,
    setPage,
    handleSerach,
  };
}
