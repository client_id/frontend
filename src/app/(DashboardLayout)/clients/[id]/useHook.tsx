"use client";
import { useState, useContext, useEffect } from "react";
import { useParams, useRouter } from "next/navigation";
import { ContextNotfication } from "@providers/ContextApiProvider/contextNotfication";

import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";

import serviceApi, {
  Client,
  PayloadClient,
  ClientProgress,
} from "@services/client";
import serviceUsers, { User } from "@services/user";

export default function StationsHook() {
  const { setSnackbar, setLoading } = useContext(ContextNotfication);
  const params = useParams();
  const paramsId = params.id as string;
  const router = useRouter();

  const [openDialog, setOpenDialog] = useState({
    open: false,
    type: "",
  });

  let initForm: PayloadClient = {
    id: 0,
    name: "",
    position: "",
    company: "",
    industry_type: "",
    phone: "",
    email: "",
    source_lit: "",
    state: "New",
    sales_id: 0,
  };

  let initFormProgress: ClientProgress = {
    state: "New",
    date: new Date().toISOString().split("T")[0],
    notes: "",
  };

  const [formProgress, setFormProgress] =
    useState<ClientProgress>(initFormProgress);
  const [form, setForm] = useState<PayloadClient>(initForm);
  const [error, setError] = useState<any>("");

  const client = useQuery(
    ["client-detail"],
    async () => serviceApi.detail(Number(paramsId)),
    {
      onSuccess(data) {
        setForm(data);
      },
      onError: (error: any) =>
        setSnackbar({
          open: true,
          text: error.data.message,
          variant: "error",
        }),
    }
  );

  const clientProgress = useQuery(
    ["client-progress"],
    async () => serviceApi.getProgress(Number(paramsId)),
    {
      onError: (error: any) =>
        setSnackbar({
          open: true,
          text: error.data.message,
          variant: "error",
        }),
    }
  );

  const updateData = useMutation(
    async (data: PayloadClient) => serviceApi.create(data),
    {
      onSuccess: (v) => onApiSuccess("Data updated successfully", v.id),
      onError: (error: any) => validate(error),
    }
  );

  const users = useQuery(["users"], async () => serviceUsers.get(), {
    // onSuccess: (res) => {
    //   console.log(res);
    // },
    onError: (error: any) =>
      setSnackbar({
        open: true,
        text: error.data.message,
        variant: "error",
      }),
  });

  function handleChange(e: any) {
    const { name, value } = e.target;

    setForm((f) => ({
      ...f,
      [name]: value,
    }));

    setError((f: any) => ({
      ...f,
      [name]: "",
    }));
  }

  function onApiSuccess(message: string, id: number) {
    // queryClient.invalidateQueries({ queryKey: ["machines"] });
    setSnackbar({
      open: true,
      text: message,
    });
    window.location.reload();
  }

  function validate(res: any) {
    if (res.status === 422) {
      const newError = { ...error };

      res.data.errors.map((item: any) => {
        newError[item.field] = item.message;
      });

      setError(newError);
      setSnackbar({
        open: true,
        text: "Unprocessable Content",
        variant: "error",
      });
    } else {
      setSnackbar({
        open: true,
        text: res.data.message,
        variant: "error",
      });
    }
  }

  function onSubmitDialog(e: any) {
    e.preventDefault();
    updateData.mutate(form);
  }

  let loading = updateData.isLoading;

  useEffect(() => {
    setLoading(loading);
  }, [loading, setLoading]);

  return {
    client: client.data,
    users: users.data
      ?.filter((item) => !item.deleted && item.role !== "Admin")
      .map((item: User) => ({
        value: item.id,
        label: item.username,
      })),
    clientProgress: clientProgress.data,
    openDialog,
    setOpenDialog,
    initFormProgress,
    form,
    setForm,
    error,
    formProgress,
    setFormProgress,
    handleChange,
    onSubmitDialog,
  };
}
