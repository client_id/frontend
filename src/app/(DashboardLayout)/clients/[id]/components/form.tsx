// Mui Components
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";

// Custom Components
import FormDialog from "@components/Dialog";
import DialogWarning from "@components/Dialog/DialogWarning";

import useHook, { Props } from "./useHook";

export default function Form(props: Props) {
  const PAGE_TITLE = "Client";

  const hooks = useHook(props);

  console.log(hooks.form.date);

  return (
    <FormDialog
      open={props.openDialog.open}
      title={props.openDialog.type + " " + PAGE_TITLE}
      textOnAgree="Confirm"
      minWidth={450}
      handleClose={hooks.handleCloseDialog}
      functionOnAgree={hooks.onSubmitDialog}
      disable={hooks.loading}
      isFormDialog={["Create", "Update"].includes(props.openDialog.type)}
    >
      <Grid
        container
        spacing={2}
        sx={{ width: "100%", mt: 2 }}
        justifyContent="center"
      >
        {["Update"].includes(props.openDialog.type) ? (
          <Grid container item lg={12} md={12} xs={12} rowSpacing={1}>
            <Grid container item alignItems="center">
              <Grid item lg={4} md={4} xs={4}>
                <Typography>State</Typography>
              </Grid>
              <Grid
                item
                lg={8}
                md={8}
                xs={8}
                sx={{ display: "flex", alignItems: "center" }}
              >
                :
                <FormControl
                  fullWidth
                  // error={hooks.error.type ? true : false}
                >
                  <Select
                    displayEmpty
                    size="small"
                    name="state"
                    value={hooks.form.state}
                    onChange={hooks.handleChange}
                    sx={{ ml: 2 }}
                  >
                    <MenuItem value={hooks.form.state} disabled>
                      <em>Choose state</em>
                    </MenuItem>
                    <MenuItem value="New">New</MenuItem>
                    <MenuItem value="Folup">Folup</MenuItem>
                    <MenuItem value="Need Presentation">
                      Need Presentation
                    </MenuItem>
                    <MenuItem value="Need Visit">Need Visit</MenuItem>
                    <MenuItem value="Need Demo">Need Demo</MenuItem>
                    <MenuItem value="Quotation">Quotation</MenuItem>
                    <MenuItem value="Transaction">Transaction</MenuItem>
                    <MenuItem value="Done">Done</MenuItem>
                    <MenuItem value="Failed">Failed</MenuItem>
                  </Select>
                  {/* {hooks.error.type ? (
                    <FormHelperText>{hooks.error.type}</FormHelperText>
                  ) : (
                    ""
                  )} */}
                </FormControl>
              </Grid>
            </Grid>
            <Grid container item alignItems="center">
              <Grid item lg={4} md={4} xs={4}>
                <Typography>Date</Typography>
              </Grid>
              <Grid
                item
                lg={8}
                md={8}
                xs={8}
                sx={{ display: "flex", alignItems: "center" }}
              >
                :
                <TextField
                  fullWidth
                  variant="outlined"
                  size="small"
                  name="date"
                  placeholder="Enter date"
                  value={hooks.form.date}
                  onChange={hooks.handleChange}
                  error={hooks.error.date ? true : false}
                  helperText={hooks.error.date}
                  sx={{ ml: 2 }}
                  type="date"
                />
              </Grid>
            </Grid>
            <Grid container item alignItems="center">
              <Grid item lg={4} md={4} xs={4}>
                <Typography>Notes</Typography>
              </Grid>
              <Grid
                item
                lg={8}
                md={8}
                xs={8}
                sx={{ display: "flex", alignItems: "center" }}
              >
                :
                <TextField
                  fullWidth
                  variant="outlined"
                  size="small"
                  name="notes"
                  placeholder="Enter notes"
                  value={hooks.form.notes}
                  onChange={hooks.handleChange}
                  error={hooks.error.notes ? true : false}
                  helperText={hooks.error.notes}
                  sx={{ ml: 2 }}
                />
              </Grid>
            </Grid>
          </Grid>
        ) : (
          <></>
        )}
      </Grid>
    </FormDialog>
  );
}
