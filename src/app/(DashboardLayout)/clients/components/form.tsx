// Mui Components
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";

// Custom Components
import FormDialog from "@components/Dialog";
import DialogWarning from "@components/Dialog/DialogWarning";

import useHook, { Props } from "./useHook";

export default function Form(props: Props) {
  const PAGE_TITLE = "Downtime";

  const hooks = useHook(props);

  return (
    <FormDialog
      open={props.openDialog.open}
      title={props.openDialog.type + " " + PAGE_TITLE}
      textOnAgree="Confirm"
      minWidth={450}
      handleClose={hooks.handleCloseDialog}
      functionOnAgree={hooks.onSubmitDialog}
      disable={hooks.loading}
      isFormDialog={["Create", "Update"].includes(props.openDialog.type)}
    >
      <Grid
        container
        spacing={2}
        sx={{ width: "100%", mt: 2 }}
        justifyContent="center"
      >
        <DialogWarning name={hooks.form.name} />
      </Grid>
    </FormDialog>
  );
}
