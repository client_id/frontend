"use client";

import Link from "next/link";
import PageContainer from "@/app/(DashboardLayout)/components/container/PageContainer";
import DashboardCard from "@/app/(DashboardLayout)/components/shared/DashboardCard";
import CustomTextField from "@/app/(DashboardLayout)/components/forms/theme-elements/CustomTextField";

import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Grid,
  Stack,
  Select,
  MenuItem,
  FormControl,
  Button,
} from "@mui/material";

// import Form from "./components/form";
import useHook from "./useHook";

const ClientPage = () => {
  const hooks = useHook();

  return (
    <PageContainer title="Client Add Page" description="this is Client page">
      <DashboardCard title="New Client">
        <Box sx={{ overflow: "auto", width: { xs: "280px", sm: "auto" } }}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={4}>
              <Box>
                <Typography
                  variant="subtitle1"
                  fontWeight={600}
                  component="label"
                  htmlFor="name"
                >
                  Name
                </Typography>
                <CustomTextField
                  size="small"
                  variant="outlined"
                  fullWidth
                  name="name"
                  placeholder="Enter name"
                  value={hooks.form.name}
                  onChange={hooks.handleChange}
                  error={hooks.error.name ? true : false}
                  helperText={hooks.error.name}
                />
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box>
                <Typography
                  variant="subtitle1"
                  fontWeight={600}
                  component="label"
                  htmlFor="position"
                >
                  Position
                </Typography>
                <CustomTextField
                  size="small"
                  variant="outlined"
                  fullWidth
                  name="position"
                  placeholder="Enter position"
                  value={hooks.form.position}
                  onChange={hooks.handleChange}
                  error={hooks.error.position ? true : false}
                  helperText={hooks.error.position}
                />
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box>
                <Typography
                  variant="subtitle1"
                  fontWeight={600}
                  component="label"
                  htmlFor="company"
                >
                  Company
                </Typography>
                <CustomTextField
                  size="small"
                  variant="outlined"
                  fullWidth
                  name="company"
                  placeholder="Enter company"
                  value={hooks.form.company}
                  onChange={hooks.handleChange}
                  error={hooks.error.company ? true : false}
                  helperText={hooks.error.company}
                />
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box>
                <Typography
                  variant="subtitle1"
                  fontWeight={600}
                  component="label"
                  htmlFor="industry_type"
                >
                  Industry Type
                </Typography>
                <CustomTextField
                  size="small"
                  variant="outlined"
                  fullWidth
                  name="industry_type"
                  placeholder="Enter industry type"
                  value={hooks.form.industry_type}
                  onChange={hooks.handleChange}
                  error={hooks.error.industry_type ? true : false}
                  helperText={hooks.error.industry_type}
                />
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box>
                <Typography
                  variant="subtitle1"
                  fontWeight={600}
                  component="label"
                  htmlFor="phone"
                >
                  Phone
                </Typography>
                <CustomTextField
                  size="small"
                  variant="outlined"
                  fullWidth
                  name="phone"
                  placeholder="Enter phone number"
                  value={hooks.form.phone}
                  onChange={hooks.handleChange}
                  error={hooks.error.phone ? true : false}
                  helperText={hooks.error.phone}
                />
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box>
                <Typography
                  variant="subtitle1"
                  fontWeight={600}
                  component="label"
                  htmlFor="email"
                >
                  Email
                </Typography>
                <CustomTextField
                  size="small"
                  variant="outlined"
                  fullWidth
                  name="email"
                  placeholder="Enter email"
                  value={hooks.form.email}
                  onChange={hooks.handleChange}
                  error={hooks.error.email ? true : false}
                  helperText={hooks.error.email}
                />
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box>
                <Typography
                  variant="subtitle1"
                  fontWeight={600}
                  component="label"
                  htmlFor="source_lit"
                >
                  Source Lit
                </Typography>

                <FormControl
                  fullWidth
                  // error={hooks.error.type ? true : false}
                >
                  <Select
                    displayEmpty
                    size="small"
                    name="source_lit"
                    value={hooks.form.source_lit}
                    onChange={hooks.handleChange}
                  >
                    <MenuItem value="" disabled>
                      <em>Choose source</em>
                    </MenuItem>
                    <MenuItem value="Web">Web</MenuItem>
                    <MenuItem value="Linkedin">Linkedin</MenuItem>
                    <MenuItem value="Instagram">Instagram</MenuItem>
                    <MenuItem value="From Sales">From Sales</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box>
                <Typography
                  variant="subtitle1"
                  fontWeight={600}
                  component="label"
                  htmlFor="sales"
                >
                  Sales
                </Typography>
                <FormControl
                  fullWidth
                  // error={hooks.error.type ? true : false}
                >
                  <Select
                    displayEmpty
                    size="small"
                    name="sales_id"
                    value={hooks.form.sales_id}
                    onChange={hooks.handleChange}
                  >
                    <MenuItem value={0} disabled>
                      <em>Choose sales</em>
                    </MenuItem>
                    {hooks.users?.map(
                      (item: { value: number; label: string }) => (
                        <MenuItem value={item.value} key={item.value}>
                          {item.label}
                        </MenuItem>
                      )
                    )}
                  </Select>
                  {/* {hooks.error.type ? (
                    <FormHelperText>{hooks.error.type}</FormHelperText>
                  ) : (
                    ""
                  )} */}
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Box>
                <Typography
                  variant="subtitle1"
                  fontWeight={600}
                  component="label"
                  htmlFor="state"
                >
                  State
                </Typography>
                <FormControl
                  fullWidth
                  // error={hooks.error.type ? true : false}
                >
                  <Select
                    displayEmpty
                    size="small"
                    name="state"
                    value={hooks.form.state}
                    onChange={hooks.handleChange}
                  >
                    <MenuItem value="" disabled>
                      <em>Choose state</em>
                    </MenuItem>
                    <MenuItem value="New">New</MenuItem>
                    <MenuItem value="Folup">Folup</MenuItem>
                    <MenuItem value="Need Presentation">
                      Need Presentation
                    </MenuItem>
                    <MenuItem value="Need Visit">Need Visit</MenuItem>
                    <MenuItem value="Need Demo">Need Demo</MenuItem>
                    <MenuItem value="Quotation">Quotation</MenuItem>
                    <MenuItem value="Transaction">Transaction</MenuItem>
                    <MenuItem value="Done">Done</MenuItem>
                    <MenuItem value="Failed">Failed</MenuItem>
                  </Select>
                  {/* {hooks.error.type ? (
                    <FormHelperText>{hooks.error.type}</FormHelperText>
                  ) : (
                    ""
                  )} */}
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box width={200}>
                <Button
                  color="primary"
                  variant="contained"
                  size="large"
                  fullWidth
                  // type="submit"
                  onClick={hooks.onSubmitDialog}
                >
                  Save
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </DashboardCard>
    </PageContainer>
  );
};

export default ClientPage;
