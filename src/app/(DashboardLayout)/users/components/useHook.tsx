"use client";

import { useState, useContext, useEffect } from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";

import { ContextNotfication } from "@providers/ContextApiProvider/contextNotfication";
import serviceApi, { PayloadUser } from "@services/user";

export interface Props {
  openDialog: {
    open: boolean;
    type: string;
  };
  onCloseDialog(e: { open: boolean; type: string }): void;
  data: PayloadUser;
}

export default function UseHook(props: Props) {
  const { setSnackbar, setLoading } = useContext(ContextNotfication);
  const queryClient = useQueryClient();

  const [form, setForm] = useState<PayloadUser>(props.data);
  const [error, setError] = useState<any>("");

  useEffect(() => {
    setForm(props.data);
  }, [props.data]);

  const createData = useMutation(
    async (data: PayloadUser) => serviceApi.create(data),
    {
      onSuccess: () => onApiSuccess("Data updated successfully"),
      onError: (error: any) => validate(error),
    }
  );

  const updateData = useMutation(
    async (data: PayloadUser) => serviceApi.update(form.id ?? 0, data),
    {
      onSuccess: () => onApiSuccess("Data updated successfully"),
      onError: (error: any) => validate(error),
    }
  );

  const deleteData = useMutation(async () => serviceApi.delete(form.id ?? 0), {
    onSuccess: () => onApiSuccess("Data deleted successfully"),
    onError: (error: any) => validate(error),
  });

  function handleChange(e: any) {
    const { name, value } = e.target;

    setForm((f) => ({
      ...f,
      [name]: value,
    }));

    setError((f: any) => ({
      ...f,
      [name]: "",
    }));
  }

  function handleCloseDialog() {
    props.onCloseDialog({ open: false, type: "" });
    setForm(props.data);
    setError("");
  }

  function onApiSuccess(message: string) {
    queryClient.invalidateQueries({ queryKey: ["users"] });
    handleCloseDialog();
    setSnackbar({
      open: true,
      text: message,
    });
  }

  function validate(res: any) {
    if (res.status === 422) {
      const newError = { ...error };

      res.data.errors.map((item: any) => {
        newError[item.field] = item.message;
      });

      setError(newError);
      setSnackbar({
        open: true,
        text: "Unprocessable Content",
        variant: "error",
      });
    } else {
      setSnackbar({
        open: true,
        text: res.data.message,
        variant: "error",
      });
    }
  }

  function onSubmitDialog(e: any) {
    e.preventDefault();
    if (["Create", "Update"].includes(props.openDialog.type)) {
      switch (props.openDialog.type) {
        case "Create":
          createData.mutate(form);
          break;
        case "Update":
          updateData.mutate(form);
          break;
      }
    } else {
      deleteData.mutate();
    }
  }

  let loading =
    createData.isLoading || updateData.isLoading || deleteData.isLoading;

  useEffect(() => {
    setLoading(loading);
  }, [loading, setLoading]);

  return {
    form,
    error,
    handleChange,
    onSubmitDialog,
    handleCloseDialog,
    loading,
  };
}
