// Mui Components
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";

// Custom Components
import FormDialog from "@components/Dialog";
import DialogWarning from "@components/Dialog/DialogWarning";

import useHook, { Props } from "./useHook";

export default function Form(props: Props) {
  const PAGE_TITLE = "User";

  const hooks = useHook(props);

  return (
    <FormDialog
      open={props.openDialog.open}
      title={props.openDialog.type + " " + PAGE_TITLE}
      textOnAgree="Confirm"
      minWidth={450}
      handleClose={hooks.handleCloseDialog}
      functionOnAgree={hooks.onSubmitDialog}
      disable={hooks.loading}
      isFormDialog={["Create", "Update"].includes(props.openDialog.type)}
    >
      <Grid
        container
        spacing={2}
        sx={{ width: "100%", mt: 2 }}
        justifyContent="center"
      >
        {["Create", "Update"].includes(props.openDialog.type) ? (
          <Grid container item lg={12} md={12} xs={12} rowSpacing={1}>
            <Grid container item alignItems="center">
              <Grid item lg={4} md={4} xs={4}>
                <Typography>Username</Typography>
              </Grid>

              <Grid
                item
                lg={8}
                md={8}
                xs={8}
                sx={{ display: "flex", alignItems: "center" }}
              >
                :
                <TextField
                  fullWidth
                  variant="outlined"
                  size="small"
                  name="username"
                  placeholder="Enter username"
                  value={hooks.form.username}
                  onChange={hooks.handleChange}
                  error={hooks.error.username ? true : false}
                  helperText={hooks.error.username}
                  sx={{ ml: 2 }}
                />
              </Grid>
            </Grid>
            <Grid container item alignItems="center">
              <Grid item lg={4} md={4} xs={4}>
                <Typography>Role</Typography>
              </Grid>
              <Grid
                item
                lg={8}
                md={8}
                xs={8}
                sx={{ display: "flex", alignItems: "center" }}
              >
                :
                <FormControl
                  fullWidth
                  sx={{ ml: 2 }}
                  error={hooks.error.type ? true : false}
                >
                  <Select
                    displayEmpty
                    inputProps={{ "aria-label": "Without label" }}
                    size="small"
                    name="role"
                    value={hooks.form.role}
                    onChange={hooks.handleChange}
                  >
                    <MenuItem value="" disabled>
                      <em>Choose role</em>
                    </MenuItem>
                    <MenuItem value="Admin">Admin</MenuItem>
                    <MenuItem value="Sales">Sales</MenuItem>
                  </Select>
                  {hooks.error.roleId ? (
                    <FormHelperText>{hooks.error.roleId}</FormHelperText>
                  ) : (
                    ""
                  )}
                </FormControl>
              </Grid>
            </Grid>
            <Grid container item alignItems="center">
              <Grid item lg={4} md={4} xs={4}>
                <Typography>Password</Typography>
              </Grid>
              <Grid
                item
                lg={8}
                md={8}
                xs={8}
                sx={{ display: "flex", alignItems: "center" }}
              >
                :
                <TextField
                  fullWidth
                  variant="outlined"
                  size="small"
                  name="password"
                  placeholder="Enter password"
                  value={hooks.form.password}
                  onChange={hooks.handleChange}
                  error={hooks.error.password ? true : false}
                  helperText={hooks.error.password}
                  sx={{ ml: 2 }}
                  type="password"
                />
              </Grid>
            </Grid>
            {props.openDialog.type === "Update" && (
              <Grid
                item
                lg={12}
                md={12}
                xs={12}
                sx={{ display: "flex", justifyContent: "end", pr: 1 }}
              >
                <Typography variant="caption">
                  Keep password empty if doesn&apos;t want to change it
                </Typography>
              </Grid>
            )}
          </Grid>
        ) : (
          props.openDialog.type === "Delete" && (
            <DialogWarning name={hooks.form.username} />
          )
        )}
      </Grid>
    </FormDialog>
  );
}
