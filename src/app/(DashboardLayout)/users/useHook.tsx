"use client";
import { useState, useContext } from "react";
import { ContextNotfication } from "@providers/ContextApiProvider/contextNotfication";

import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";

import serviceApi, { User, PayloadUser } from "@services/user";

export default function StationsHook() {
  const { setSnackbar, setLoading } = useContext(ContextNotfication);

  const [openDialog, setOpenDialog] = useState({
    open: false,
    type: "",
  });

  let initForm: PayloadUser = {
    id: 0,
    password: "",
    username: "",
    role: "Sales",
  };

  const [form, setForm] = useState<PayloadUser>(initForm);

  const users = useQuery(["users"], async () => serviceApi.get(), {
    // onSuccess: (res) => {
    //   console.log(res);
    // },
    onError: (error: any) =>
      setSnackbar({
        open: true,
        text: error.data.message,
        variant: "error",
      }),
  });

  return {
    users: users.data,
    openDialog,
    setOpenDialog,
    initForm,
    form,
    setForm,
  };
}
