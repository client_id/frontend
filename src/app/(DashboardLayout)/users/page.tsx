"use client";

import Link from "next/link";
import PageContainer from "@/app/(DashboardLayout)/components/container/PageContainer";
import DashboardCard from "@/app/(DashboardLayout)/components/shared/DashboardCard";

import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Button,
} from "@mui/material";

import Form from "./components/form";
import useHook from "./useHook";

const ClientAddPage = () => {
  const hooks = useHook();

  return (
    <PageContainer title="Client Page" description="this is Client page">
      <DashboardCard title="Daftar User">
        <Box sx={{ overflow: "auto", width: { xs: "280px", sm: "auto" } }}>
          <Button
            variant="contained"
            disableElevation
            color="primary"
            onClick={() => hooks.setOpenDialog({ open: true, type: "Create" })}
          >
            Tambah User
          </Button>
          <Table
            aria-label="simple table"
            sx={{
              whiteSpace: "nowrap",
              mt: 2,
            }}
          >
            <TableHead>
              <TableRow>
                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    No
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    Username
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    Role
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography
                    variant="subtitle2"
                    fontWeight={600}
                    align="center"
                  >
                    Action
                  </Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {hooks.users
                ?.filter((item) => !item.deleted)
                .map((item, index) => (
                  <TableRow key={item.id} hover>
                    <TableCell>
                      <Typography
                        sx={{
                          fontSize: "15px",
                          fontWeight: "500",
                        }}
                      >
                        {index + 1}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography
                        color="textSecondary"
                        variant="subtitle2"
                        fontWeight={400}
                      >
                        {item.username}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography
                        color="textSecondary"
                        variant="subtitle2"
                        fontWeight={400}
                      >
                        {item.role}
                      </Typography>
                    </TableCell>

                    <TableCell align="center">
                      <Button
                        variant="contained"
                        onClick={(e) => {
                          e.stopPropagation();
                          e.preventDefault();
                          hooks.setForm({
                            id: item.id,
                            username: item.username,
                            role: item.role,
                            password: "",
                          });
                          hooks.setOpenDialog({ open: true, type: "Update" });
                        }}
                        disableElevation
                        sx={{ mr: 2 }}
                      >
                        Update
                      </Button>

                      <Button
                        variant="contained"
                        onClick={(e) => {
                          e.stopPropagation();
                          e.preventDefault();
                          hooks.setForm({
                            id: item.id,
                            username: item.username,
                            role: item.role,
                            password: "",
                          });
                          hooks.setOpenDialog({ open: true, type: "Delete" });
                        }}
                        disableElevation
                        color="error"
                      >
                        Delete
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Box>
      </DashboardCard>
      <Form
        data={hooks.form}
        openDialog={hooks.openDialog}
        onCloseDialog={(e) => {
          hooks.setOpenDialog(e);
          hooks.setForm(hooks.initForm);
        }}
      />
    </PageContainer>
  );
};

export default ClientAddPage;
