"use client";
import { useState, useContext } from "react";
import { ContextNotfication } from "@providers/ContextApiProvider/contextNotfication";

import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";

import serviceApi, { Client } from "@services/client";
import serviceUser from "@services/user";
import { userAuthenticated } from "@/utils/auth";

export default function StationsHook() {
  const { setSnackbar, setLoading } = useContext(ContextNotfication);

  const [clientData, setClientData] = useState<Client[]>([]);

  const isAdmin = userAuthenticated.role === "Admin";
  const idUserLogin = !isAdmin ? userAuthenticated.id : 0;

  const stateCount = useQuery(
    ["state-count"],
    async () => serviceApi.getStateCount(idUserLogin),
    {
      onError: (error: any) =>
        setSnackbar({
          open: true,
          text: error.data.message,
          variant: "error",
        }),
    }
  );

  const userClientCount = useQuery(
    ["user-client-count"],
    async () => serviceApi.getUserClientCount(idUserLogin),
    {
      // onSuccess: (res) => {
      //   console.log(res);
      // },
      onError: (error: any) =>
        setSnackbar({
          open: true,
          text: error.data.message,
          variant: "error",
        }),
    }
  );

  const topUsers = useQuery(["top-users"], async () => serviceUser.topUsers(), {
    // onSuccess: (res) => {
    //   console.log(res);
    // },
    onError: (error: any) =>
      setSnackbar({
        open: true,
        text: error.data.message,
        variant: "error",
      }),
  });

  const clients = useQuery(["clients"], async () => serviceApi.get(), {
    onSuccess: (res) => {
      if (isAdmin) {
        setClientData(res);
      } else {
        let v = res.filter((item) => item.sales_id === userAuthenticated.id);
        setClientData(v);
      }
    },
  });

  return {
    stateCount: stateCount.data,
    userClientCount: userClientCount.data,
    topUsers: topUsers.data,
    clients: clientData?.slice(0, 5),
  };
}
