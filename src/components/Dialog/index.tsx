import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import IconButton from "@mui/material/IconButton";
import Box from "@mui/material/Box";

// MUI Icons
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";

interface props {
  open: boolean;
  handleClose: any;
  functionOnAgree: any;
  title: string;
  textOnAgree: string;
  children: React.ReactNode;
  minWidth?: number;
  disable: boolean;
  isFormDialog: boolean;
}

export default function FormDialog(props: props) {
  return (
    <Dialog open={props.open} onClose={props.handleClose}>
      <DialogTitle
        sx={{
          minWidth: props.minWidth ?? 600,
          borderBottom: "1px solid #EEEEEE",
          py: 2,
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          fontWeight: 600,
          textTransform: "capitalize",
        }}
      >
        {props.title}
        <IconButton onClick={props.handleClose}>
          <CloseRoundedIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>{props.children}</DialogContent>
      <DialogActions sx={{ pr: 5, pt: 2, pb: 5 }}>
        <Box sx={{ width: 200 }}>
          <Button
            className="btn"
            fullWidth
            variant="contained"
            onClick={props.functionOnAgree}
            style={{ fontWeight: "bold", fontSize: 16 }}
            disabled={props.disable}
          >
            {props.textOnAgree}
          </Button>
        </Box>
      </DialogActions>
    </Dialog>
  );
}
