// MUI Component
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

// MUI Icon
import WarningIcon from "@mui/icons-material/Warning";
import ErrorOutlineOutlinedIcon from "@mui/icons-material/ErrorOutlineOutlined";

interface Props {
  name: string | undefined;
  message?: string;
}

export default function DialogWarning(props: Props) {
  return (
    <Grid container item justifyContent="center">
      <Grid item xs={12} sx={{ textAlign: "center" }}>
        <ErrorOutlineOutlinedIcon color="warning" sx={{ fontSize: 100 }} />
      </Grid>
      <Grid item>
        <Typography textAlign="center" sx={{ maxWidth: 320 }}>
          {props.message ?? "Are you sure you want to delete"}
          <Typography
            component="span"
            display="inline"
            color="red"
            paddingX={0.5}
          >
            {props.name}
          </Typography>
          ?
        </Typography>
      </Grid>
    </Grid>
  );
}
